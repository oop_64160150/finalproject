/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anawin.finalproject;

import java.io.Serializable;

/**
 *
 * @author anawi
 */
public class PizzaHub implements Serializable{
    private String menuname;
    private int stock;
    private int price;
    private int purchase;
    public PizzaHub(String menuname,int stock,int price){
        this.menuname = menuname;
        this.stock = stock;
        this.price = price;
        this.purchase = 0;
    }


    public String print(){
        return(menuname+" have "+stock+" in stock "+"price "+price);
    }
    @Override
    public String toString() {
        return(menuname+" have "+stock+" in stock "+"price "+price);
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setPurchase(int purchase) {
        this.purchase = purchase;
    }
    public int getStock(){
        return stock;
    }

    public String getMenuname() {
        return menuname;
    }

    public int getPrice() {
        return price;
    }

    public int getPurchase() {
        return purchase;
    }
    public int itemSelect(){
        return stock = stock-1;    
    }
    public int itemIn(){
        return purchase = purchase+1;
    }
    public int calculate(){
        int total = 0;
        total = purchase*price;
        return total;
    }
    public int itemFallback(){
        return stock = stock +1;
    }
    public int itemOut(){
        return purchase = purchase-1;
    }
    public int defualtPurchase(){
        this.purchase = 0;
        return purchase; 
    }
}
